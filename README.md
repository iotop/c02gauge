# C02 Gauge Chart

The GitLab repository for all development around the C02 Gauge Chart, which directly coincides with the iotsensors
web application.

# Purpose 
This project will be used/deployed into the iotsensors web application. The graph will then dynamiclly allow users to select a c02 device based on the 
given iD value, and be able to view the payload output in realtime within a Google Gauge chart. In order to implement the Gauge graph, the
following files must be used:

````
connect.inc.php
error.html.php
gauge_data.php
CarbonGauge.php

````

Each file has a specific function/relationship with the main file, CarbonGauge.php.


# Connecting To The Database and Host
Step 1: Download the files and load up the connect.inc so you will see the following content:

![connectFile](https://gitlab.com/iotop/c02gauge/-/raw/master/images/connectFile.PNG) 


Step 2: Once done, you must have the connect file included at the top of each other file within a php container:

````
<?php
	include("connect.inc.php");
?>
````
Note: you won't need this for the error.html file as it's only used to diplay an error in connection.

Step 3: Next, be sure to access the sensitive repository containing the phpmyadmin login information. This info can be found within 
our shared iotop GitLab, of which will allow us to access the database. Once found, replace the dummy strings with the given 
credidentials:


![securityStuff](https://gitlab.com/iotop/c02gauge/-/raw/master/images/securityStuff.PNG) 


You should now have an established connection to the database once loaded.

# Accessing The Data With JSON and Ajax
The crucial file that will allow our data and payloads to be displayed is the gauge_data.php file. This file will be accessed through the use of
an ajax callback function and json encoding. 

Step 1: First, we need to create a set request parameter to include within our given query. The parameter is needed so that
if we build more c02 devices and add them to the database, we wont need to create a multitude of querys to reference the
 new ids. The request parameter is set as such: 

```
if(isset($_REQUEST['dev_id']))
	{   	
		$variable = $_REQUEST['dev_id'];
		$query = "SELECT * FROM data WHERE app_id = 'op_roomsensors' AND dev_id LIKE '$variable' GROUP by dev_id DESC Limit 1";	
	}
```
This will check that the request has been set and will then have it set as a variable withint our query. The variable should
 alter each time the user selects a different device in the drop down menu.

Step 2: We now need to call a json_encode so that the payload will be displayed during the ajax callback:

````
	$posts = $pdo->query($query);
	$statement = $pdo->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();

	foreach($posts as $row)
	{
		$v = intval($row['payload']);
	}
	header('Content-type: application/json');
	echo json_encode($v);  	
````

The above code loops through the payload field within the database, and will then be stored as $v variable,
of which will be encoded as a json data type.

Step 3: The ajax call will then be implemeneted within the main file. Load up the CarbonGauge file so that you can see
the below display:

````
var jsonData = $.ajax({
	url: "gauge_data.php",
		type: 'POST',
		dataType: 'JSON',	
		data: ({dev_id: $('#Device').val()}),
		async: false,
	});
````
These are the general settings for our ajax callbacks, notice that the data is set with the query variable. This is so that if the menu
 selection from #Device matches the dev_id, then the query should adjust accordingly. A second ajax function is called at the end of CarbonGauge called
 "getData" of which is used to display the payload value in its formatted state:
 
````
success: function (data) 
	{
		gaugeData.setValue(0, 1, data);
	    	chart.draw(gaugeData, gaugeOptions);
		formatter.format(gaugeData,1);
		}						
	});
````
The ajax call here is pretty much the same, but will have a success function to show the data.
IMPORTANT: You must ensure that setInterval is called after the ajax call, as if it is in the wrong line the server will overload
with too many requests.

# How To Create/Display The Gauge Chart 
Load up the CarbonGauge file to view the implemented code, you'll notice that the drawChart function is created all inside the
 google chart declaration field. This is so that the graph will change as the menu select changes. 

Step 1: In order to build the c02 Gauge, we have to incorperate an API into the main php file along with the necessary script
links so we can use and access the required code formats:

```
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<link rel="stylesheet" href="scripts/styles.css">
	<div id="chart_div" style="width:280px; height: 180px;"></div>
	<script type="text/javascript">
````
We will also need to declare the chart object that we wish to create. For this graph, however, we will be having the google
object as a large function that holds all the drawChart assests. As the code is too large to display, please review the CarbonGauge.php
file as the code comments explain how the container is set up.

Step 2: We then need to ensure that we have declared both the data columns and the display options. These will be our essential
chart settings so we can control the min-max values, colors, and data that gets displayed. For specific c02 values, please refer to
the node repo provided below:

````
var gaugeData = new google.visualization.DataTable(jsonData);
gaugeData.addColumn('string', 'Carbon Dioxide');
gaugeData.addColumn('number', 'Value');
gaugeData.addRows([
[$("#Device").val(), 0]]);

var gaugeOptions = {
			min: 0, 
			max: 2500, 
			greenFrom: 0, 
			greenTo: 750, 
			yellowFrom: 750, 
			yellowTo: 1500,
			redFrom: 1500, 
			redTo: 2500, 
			minorTicks: 10
		};



````
[Google Gauge Chart Link](https://developers.google.com/chart/interactive/docs/gallery/gauge)

[c02 payload range values](https://gitlab.com/iotop/room-sensors/-/tree/master/node/c02-monitor)

If you are unsure about the graph settings, refer to the provided link for a guide on how to styalize the options and data.

Step 3: Once thats done, we then need to declare a formatter and a chart object to assign the data and options settings to the new chart:

````
var formatter = new google.visualization.NumberFormat({suffix: 'ppm',pattern:'#'});
	formatter.format(gaugeData,1);

var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
chart.draw(gaugeData, gaugeOptions);
````
This way, we will have the display be formatted the same as how the TTN displays the payloads. 

Step 4: Now that all the settings, display options, and database assests have been established, we then need to create the drop down menu.
For this, we need to ensure that the menu options (such as form method, div id, select id, and option values) all match the graph settings
that we have established and that the option values are exactly the same as the dev_ids that show in the things database:

````
<div id="chart_div">
	<form method = "POST">
		<select id = "Device">
			<option value="" selected disabled>Please Select A Device</option>
			<option value="co2_01">C02_01</option>
			<option value="co2_02">C02_02</option>
			<option value="co2_03">C02_03</option>
		</select>
	</form>
</div>
````
When the user selects the desired device, the Gauge will then be displayed as such:

![device1](https://gitlab.com/iotop/c02gauge/-/raw/master/images/device1.PNG) 

![device2](https://gitlab.com/iotop/c02gauge/-/raw/master/images/device2.PNG) 

![device3](https://gitlab.com/iotop/c02gauge/-/raw/master/images/device3.PNG)

The ids and payloads of course will be different once you have assigned the correct dev_ids into the menu.

# Testing the Gauge Chart
Now that all the assests are setup, we can now begin testing to ensure that the data we are getting is both correct and
displaying. 

Note: These Steps can also be used to test the carbonlevels.php file.

Step 1: Make sure you have putty installed on your device in order for you to access the kate server. If not, install
it [here](https://www.putty.org/)

Step 2: Next you'll want to establish a connection to kate by inputting the following settings

![puttySettings](https://gitlab.com/iotop/c02gauge/-/raw/master/images/puttySettings.PNG)

When you are prompted, use the credidentials for the iot kate account from our sensitive repo.

Step 3: Once a connection has been established, make sure to install the winSCP application in order to interact with the kate server
[winSCP download](https://winscp.net/eng/download.php)

Follow the install wizard and run the application. Simply use the credidentials from the putty setup.

Step 4: Next, be sure to drag all relavent files into the provided window. The example below is me using my student account
to access kate.

![winSCPSetup](https://gitlab.com/iotop/c02gauge/-/raw/master/images/winSCPSetup.PNG)

Step 5: Once that is done, open up your browser and insert the following link:
````
http://kate.ict.op.ac.nz/~opiot/
````
Then select CarbonGauge.php to open up the graph

Step 6: To ensure we are getting a response, right-click on the screen and select "inspect". Once done, navigate to the network page
and select a device. When a device is selected, click on the gauge_data.php that has appeared in the network tab and navigate to the response tab.
You should be able to now see a response similar the the image below:

![networkResponse](https://gitlab.com/iotop/c02gauge/-/raw/master/images/networkResponse.PNG)

You are now all set to use the graph.

# Optional Settings
For the user's benefits, we can modify the css style to make the page and the menu more interactive, responsive, and more
aesthetically pleasing to look at. If not, feel free to leave the menu with its default display.

Another option is that you can assign the option values with the devices location, for example:

````
<div id="chart_div">
	<form method = "POST">
	<select id = "Device">
		<option value="" selected disabled>Please Select A Device</option>
		<option value="co2_01">Location: D205</option>
````
However, this would require some testing in case this causes any display errors or interference with the database assests.

# Deploying the C02 Gauge & Data (Warning: This is not how to deploy on Dev server. Check the last section for more)
We will now deploy the c02 locally in an Ubuntu VM. It will be deployed inside a docker container.

## Prerequisites
You must have the following installed:
- VMware Player/Pro (You can also use VirtualBox)
- Ubuntu (Download iso file here: https://ubuntu.com/download)
- Docker
- Docker-compose (NOTE: You must install this seperately) 

Step 1: Once you have cloned and pulled this repo, we will need to modify the 'connect.inc.php' file **inside** the 'web' folder. 

(Hint: This is the same as the very first step in the document)
NOTE: **DO NOT** edit the 'connect.inc.php' file oustside the 'web' folder. Only the files within the docker folder will be deployed.

![correct Image](images/web_folder.PNG)

Step 2: We will now build and run using docker commands. Follow the given commands:
        
        cd ~/[location for your docker]/c02gauge
        docker-compose build
        docker-compose up -d
    
    The docker commands used builds and runs the container with a -d extensions saying to run in the background. 
    
Step 3: To Display type this link in the browser: localhost:8000

![correct Image](images/co2 display.PNG)

# Deploying on Dev server
To access the dev server, you will need to putty into it using information from the sensitive repo (https://gitlab.com/iotop/sensitive/).

NOTE: You will need to follow the reverse proxy instructions (https://gitlab.com/iotop/server/-/tree/master/nginxProxy). 
But **before** that, you will need to add the ip address and the domain to the hosts on your physical machine.

localhost:8000 will now become whatever the domain in hosts are (co2Gauge.example.com).

Lastly, remember to docker-compose build and up -d for co2 that is within the reverse proxy.

![correct Image](images/DevC02.PNG)








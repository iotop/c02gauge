<?php
    include('connect.inc.php');

    //The gauge_data file is used for general ajax/json calls within the main file.
    //this is so that the data from the database can be accessed based on what dev_id is selected.
	//A REQUEST parameter is used here to ensure that the drop down idea has been set.
	if(isset($_REQUEST['dev_id']))
		{   	
			$variable = $_REQUEST['dev_id'];
			$query = "SELECT * FROM data WHERE app_id = 'op_roomsensors' AND dev_id LIKE '$variable' GROUP by dev_id DESC Limit 1";			
			$posts = $pdo->query($query);
			$statement = $pdo->prepare($query);
			$statement->execute();
			$result = $statement->fetchAll();
			
			//the foreach loop loops through the payload that the device sends out and we decode it here
			//so that it displays in a human readable format. In this case, an integer
            foreach($posts as $row)
            {
                
				$v = intval($row['payload']);
            }
			header('Content-type: application/json');
			echo json_encode($v);
			//storing the result of the executed query		
			//set the response content type as JSON		
		}	

		else
		{	
			echo ("Error No Data");//If no data is found, then the response in the network tab of the console will display an error message
		}
		//The code quite obviously would need to include only one query, as the query would alter based on each
	//case statement (i.e. if the case is c02_02, then the query 
	//SELECT * FROM data WHERE dev_id = \$variable ORDER by dataID DESC Limit 1
	//would have the post statement equal c02_02.)




    

?>

<?php
	include("connect.inc.php");//used to establish a connection to the database
?>

<html>
  <head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!---Declaring script links and api in order to fetch the google chart display and use javascript, jquery, and ajax inside an html file---->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<link rel="stylesheet" href="scripts/styles.css"> <!---Some basic css styling for the drop down menu---->
	<div id="chart_div" style="width:280px; height: 180px;"></div>
    <script type="text/javascript">
		//Declaring the google chart object
		google.charts.load('current', {
			callback: function() 
			{
				$("#Device").change(drawChart);//the change function is used here to redraw the graph in association with the Device id
				function drawChart() {	
					var jsonData = $.ajax({//The first ajax call is used here to pull the data from the json file into the graph
						url: "gauge_data.php",
						type: 'POST',
						dataType: 'JSON',	
						data: ({dev_id: $('#Device').val()}),//the data format directly associates with the query called in gauge_data.php
						//as if the dev_id equals the menu selection (i.e. co2_01) then the query will have dev_id = co2_01
						async: true,
						});

					var gaugeData = new google.visualization.DataTable(jsonData);
					gaugeData.addColumn('string', 'Carbon Dioxide');
					gaugeData.addColumn('number', 'Value');
					gaugeData.addRows([
					[$("#Device").val(), 0]]);
					//I have removed the hardcoded values and reused variables in favour of displaying
					//the pulled information from the switch statement and drop down menu.
					//This way the display will be more fluent. 


					var gaugeOptions = {//this is the option variable for how the gauge chart is displayed
					//including values, min-max, and colors.
						min: 0, 
						max: 2500, 
						greenFrom: 0, 
						greenTo: 750, 
						yellowFrom: 750, 
						yellowTo: 1500,
						redFrom: 1500, 
						redTo: 2500, 
						minorTicks: 10
						};

					//formats the payload output as a number object and displays it as a ppm reading
					var formatter = new google.visualization.NumberFormat({suffix: 'ppm',pattern:'#'});
						formatter.format(gaugeData,1);

					//a new chart is created and calls the draw method in association with the div id we assign it.
					//this will determine how and where the chart is drawn
					var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
					chart.draw(gaugeData, gaugeOptions);

					setInterval(function getData () 
					{//a second ajax call is declared within a function so that the data being pulled from the json file
					//will then be setup for display here
						$.ajax({
						url: 'gauge_data.php',
						type: 'POST',
						dataType: 'JSON',				
						data: ({dev_id: $('#Device').val()}),
						 //if the device id matches the query rule, then the associated payload
						//will then load up
						async: false,
						success: function (data) 
						{
							gaugeData.setValue(0, 1, data);
							chart.draw(gaugeData, gaugeOptions);
							formatter.format(gaugeData,1);
							
						}
													
						});
						
					}, 300000)//setInterval is the refresh rate that we set up for how frequent we want the data to refresh for.
					//for functionality purposes, all devices that get selected are set to refresh at 1 minute as to not overload the server with requests
					//and to simulate a data readout in real-time
												
			};		

		}, packages: ['gauge']});
	
			
		</script>
  </head>
  <body>
	<div id="chart_div">
		<form method = "POST">
			<select id = "Device">
				<option value="" selected disabled>Please Select A Device</option>
				<!-----Once a device has been added into the database and the TTN, we then add that as an 
				option into the dropdown menu-->
				<option value="co2_01">C02_01</option>
				<option value="co2_02">C02_02</option>
				<option value="co2_03">C02_03</option>
			</select>
		</form>
	</div>

  </body>
</html>
<?php
//Switch statement incorperated after the drop down is created, as not possible to fetch the user selected value
//before the drop down id is called
	if ($_POST)
	{
		$x = $_POST['Device'];
		switch ($x) 
			{
				case "c02_02":
					$posts = $pdo->query($query1);
					$statement = $pdo->prepare($query1);
					$statement->execute();
					$result = $statement->fetchAll();
					break;
				case "c02_01":
					$posts = $pdo->query($query2);
					$statement = $pdo->prepare($query2);
					$statement->execute();
					$result = $statement->fetchAll();
					break;
				case "c02_03":
					$posts = $pdo->query($query3);
					$statement = $pdo->prepare($query3);
					$statement->execute();
					$result = $statement->fetchAll();
					break;
				default:
					echo "Device cannot be found";
			}
	}

?>

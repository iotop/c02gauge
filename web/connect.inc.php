<?php
	/*Modified By: Callam Josef Jackson-Sem
	Project: C02 Gauge Chart
	Description: This project is designed for web use and for displaying the latest C02 readings of each 
				 MH-Z19B c02 sensor. The display allows for the user to view each device's latest c02 reading 
				 in a modified Gauge chart using an Googles Chart API. The c02 readings are obtained through the things
				 network, of which hosts the database.
	*/
	$host = "10.118.24.32";
	$userMS = "duniot";
	$passwordMS = "here!=P@ssw0rd";
	$database = "things";
        
    // Establish connection to DB server
    try 
    {
        $pdo = new PDO("mysql:host=$host;port=3306;dbname=$database", $userMS, $passwordMS);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->exec('SET NAMES "utf8"');
    }

    catch (PDOException $e)
    {
        $error = 'Connection to the host failed!';
        include 'error.html.php';
        exit();
    }
?>

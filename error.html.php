<!--
     Dev: Callam Josef Jackson-Sem - (Callam7/jackcj1)
     Desc: The file here is used for general timeout error messages, as well as if the server overloads with too many requests
-->

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>ERROR PAGE</title>
    </head>

    <body>
        <p>
        <?php
            echo ($error . "<br>" . $e->getMessage()); 
        ?>
        </p>
    </body>
</html>
